# S-Gallery: Simplistic Photo Gallery #

S-Gallery is an extremely simplistic PHP photo gallery, which I wrote back in 2007 out of frustration that there was nothing as lightweight that just worked without need for configuration or database. S-Gallery is a very thin layer on top of the file system, giving the users convenient web interface, caching, and album descriptions. It trades functionality for simplicity and ease of use.

## Screenshots ##

![screenshot1.png](https://bitbucket.org/repo/XdzdMo/images/1365051435-screenshot1.png)

## Setup ##

* Copy S-Gallery's source to a directory. Ensure the directory is served from a PHP-enabled HTTP server (consider [XAMPP](https://www.apachefriends.org/) or [PHP's built-in web server](http://php.net/manual/en/features.commandline.webserver.php)).
* Put your photo dirs to it. S-Gallery will scan recursively the whole directory tree from within its location and will regard as an album each directory that has a **"_gal_thumb" subdirectory**.
* If you want S-Gallery to ignore some dir and not visualize it, prefix its name with "\_gal\_".
* To add description for an album, add an "_gal_info.txt" file into its directory.

## Advantages ##

* No database needed.
* No need to change the way you manage your photos. Just copy the source to your photos' root dir, mark the album dirs by creating "_gal_thumb" dirs in them, and you are done.

## Disadvantages ##

* URLs to albums are unstable, i.e. you cannot simply share an album link, since after you add or remove other albums, it might no longer point to the album you meant. Note: This can be addressed, just some time needs to be allocated. :)
* No way to give descriptions or other metainformation to individual photos.

## Contacts ##

* The repo is owned and maintained by [Svilen Marchev](https://plus.google.com/u/0/+SvilenMarchev/).