<?php

///////////////////////////////////////////////////////////
// Makes a thumbnail of a given image 
// and outputs it in the browser
///////////////////////////////////////////////////////////

header("Content-Type: image/jpeg");

include_once("inc_conf.php");
include_once("inc_image.php");

///////////////////////////////////////////////////////////


$path_orig = '../' . urldecode($_REQUEST['path_orig']);
$path_thumb = '../' . urldecode($_REQUEST['path_thumb']);
$img_orig = urldecode($_REQUEST['img_orig']);
$img_thumb = urldecode($_REQUEST['img_thumb']);

if (!CreateThumbnail($path_orig.$img_orig, $path_thumb.$img_thumb, true))
{
	$img = ImageCreateFromGif("_gal_img/np.gif");
	ImageGif($img);
	ImageDestroy($img);
}

?>
