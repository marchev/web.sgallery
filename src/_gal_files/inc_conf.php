<?php

/////////////////////////////////////////////////////////////////
//  Configuration file
/////////////////////////////////////////////////////////////////

// basic album settings
define('IMAGES_PER_ROW', 3);
define('IMAGE_CELL_WIDTH', 260);
define('IMAGE_CELL_HEIGHT', 260);


// max. thumbnail size; images with larger dimensions will be 
// resized proportionally
define('THUMBNAIL_SIZE', 250);


// files over IMAGE_MAX_SIZE bytes won't be processed at all,
// even if they are valid images
// set this value to -1 if you do not want to do such a verification
define('IMAGE_MAX_SIZE', 16*(1<<20));


// do we have to check the file extension?
// NOTE: if you set the variable to "false", you will see all 
//       VALID images in the folder - we no longer care about 
//       the file extensions :)
define('CHECK_FILE_EXT', true);

// all file extensions we shall accept in the gallery
// NOTE: whatever extension you specify here, ONLY valid images
//       will be shown in the gallery (with or without a thumbnail).
$SUPPORTED_FILE_EXT = array('.jpg','.jpeg','.gif',/*'.bmp',*/'.png');


// constants for image's filename which is printed below the thumbnail:
// if the filename is too long, how many portions (rows) of it will be printed?
define('IMAGE_FNAME_ROWS', 3);
// ..and how long will be this portion (row)?
define('IMAGE_FNAME_CHARS_PER_ROW', 40);


?>
