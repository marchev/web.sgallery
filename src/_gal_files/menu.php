<?php

///////////////////////////////////////////////////////////
// Recursively scans the subfolders of a given folder, 
// builds a tree and prints it
///////////////////////////////////////////////////////////

include_once("inc_conf.php");

///////////////////////////////////////////////////////////


class TreeNode
{
		var $str = '';	// the string of the menu item
		var $info = '';	// information about the gallery in the folder (if exists)
		var $link = '';	// where is the gallery?
		var $kids = array();	// kids of the menu item (indices in $nodes)
};

$nodes = array();
$nodes[] = new TreeNode();

function BuildTree($path, $curix)
{
	global $nodes;
	
	// is there a gallery and info for current folder
	$fname = $path."_gal_thumb/";
	if (is_dir($fname)) {
		$nodes[$curix]->link = $path;
	}
	
	$fname = $path."_gal_info.txt";
	if (is_file($fname)) {
		$fin = fopen($fname, "r");
		$nodes[$curix]->info = fread($fin,filesize($fname));
		fclose($fin);
	}
	
	// scan current folder and store all of its subfolders
	$d = dir($path);
	$tmp_kids = array();
	
	while (false !== ($entry = $d->read()))
		if ($entry != '.' && $entry != '..' && is_dir($path.$entry) && substr($entry,0,5)!="_gal_")
			$tmp_kids[] = $entry;
	$d->close();
	
	// sort kids
	sort($tmp_kids);
	
	// add all subfolders to tree
	for ($i=0; $i<count($tmp_kids); $i++) {
		$new_node = new TreeNode();
		$new_node->str = $tmp_kids[$i];
		$nodes[] = $new_node;
		$nodes[$curix]->kids[] = count($nodes)-1;
			
		BuildTree($path.$tmp_kids[$i]."/", count($nodes)-1);
	}
}

function PrintTree()
{
	global $nodes;
	
	for ($i=0; $i<count($nodes[0]->kids); $i++)
    echo get_format_node($nodes[0]->kids[$i], $i==count($nodes[0]->kids)-1);
}

function get_format_node($cur_nd, $is_last_child)
{
	global $nodes;
	$ret = '';

	if ($is_last_child)
	{
		$img='_gal_files/_gal_img/minus_l.gif';
		$style='';
	}
	else
	{
		$img='_gal_files/_gal_img/minus.gif';
		$style='background-image: url(_gal_files/_gal_img/vertical.gif);';
	}
	
	if (count($nodes[$cur_nd]->kids) == 0)
	{
		$ret = get_format_leaf($cur_nd, $is_last_child);
	}
	else
	{
		for ($i=0; $i<count($nodes[$cur_nd]->kids); $i++)
		{
			$ret .= get_format_node($nodes[$cur_nd]->kids[$i], $i==count($nodes[$cur_nd]->kids)-1);
		}

		if (!empty($nodes[$cur_nd]->link))
			$str = "<a href='". ($_SERVER["PHP_SELF"]."?folder=".($cur_nd)) ."'>". $nodes[$cur_nd]->str ."</a>";
		else
			$str = $nodes[$cur_nd]->str;
			
			
		$ret = '
			<div>
				<table border="0" cellpadding="0" cellspacing="0">
					<tbody>
					<tr>
						<td class="tr_t6" style="'. $style .'"><img alt="" src="'. $img .'"></td>
						<td class="tr_t2">'. $str .'</td>
					</tr>
					</tbody>
				</table>
				<table style="display: block;" border="0" cellpadding="0" cellspacing="0">
					<tbody>
					<tr>
						<td style="'. $style .'"><div class="tr_d5"></div></td>
						<td class="tr_t5">'. $ret .'</td>
					</tr>
					</tbody>
					</table>
				</div>';
	}
	
	return $ret;
}

function get_format_leaf($cur_nd, $is_last_child)
{
	global $nodes;
	
	if (!empty($nodes[$cur_nd]->link))
		$str = "<a href='". ($_SERVER["PHP_SELF"]."?folder=".($cur_nd)) ."'>". $nodes[$cur_nd]->str ."</a>";
	else
		$str = $nodes[$cur_nd]->str;
	
	if ($is_last_child)
	{
		$img = '_gal_files/_gal_img/hr_l.gif';
		$style = '';
	}
	else
	{
		$img = '_gal_files/_gal_img/hr.gif';
		$style = 'background-image: url(_gal_files/_gal_img/vertical.gif);';
	}
	
	$ret = '
		<div>
			<table border="0" cellpadding="0" cellspacing="0">
				<tbody>
				<tr>
					<td class="tr_t6" style="'. $style .'"><img alt="" src="'. $img .'"></td>
					<td class="tr_t2">'. $str .'</td>
				</tr>
				</tbody>
			</table>
		</div>';

	return $ret;
}

BuildTree("./", 0);
PrintTree();

?>
