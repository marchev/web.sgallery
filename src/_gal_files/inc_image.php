<?php

//////////////////////////////////////////////////////////////
// Functions for images and thumbnails
//////////////////////////////////////////////////////////////


// check if the file is an image
function is_image($file)
{
	global $SUPPORTED_FILE_EXT;
	$types = &$SUPPORTED_FILE_EXT;

	if (!is_file($file))
		return false;
	
	if (IMAGE_MAX_SIZE>=0 && filesize($file)>IMAGE_MAX_SIZE)
		return false;
	
	// check if we support this extension
	if (CHECK_FILE_EXT)
	{
		for ($i=0; $i<count($types); $i++)
			if (strtolower(substr($file, -strlen($types[$i]))) == $types[$i])
				break;
		if ($i == count($types))
			return false;
	}

	return (false !== GetImageSize($file));
}

// creates a proportional thumbnail of a given image;
// returns true on success
function CreateThumbnail($img_orig, $img_thumb, $output_in_browser=false)
{
	if (!is_image($img_orig))
		return false;

	if (false === ($img_orig_info = GetImageSize($img_orig)))
		return false;

	$w = $img_orig_info[0];
	$h = $img_orig_info[1];
	
	if ($w <= THUMBNAIL_SIZE && $h <= THUMBNAIL_SIZE)
	{
		$tn_w = $w;
		$tn_h = $h;
	}
	else
	{
		if ($w > $h)
		{
			$x_ratio = THUMBNAIL_SIZE / $w;
			$tn_w = THUMBNAIL_SIZE;
			$tn_h = ceil($x_ratio * $h);
		}
		else
		{
			$y_ratio = THUMBNAIL_SIZE / $h;
			$tn_w = ceil($y_ratio * $w);
			$tn_h = THUMBNAIL_SIZE;
		}
	}
	
	// handle image according to its type
	// currently supported file formats are: jpeg, gif and png
	
	$suffix = '';
	if ($img_orig_info[2] == 1) $suffix='gif';
	if ($img_orig_info[2] == 2) $suffix='jpeg';
	if ($img_orig_info[2] == 3) $suffix='png';
	if ($img_orig_info[2] == 6)
	{	/*  bmp; use shell commands "bmptoppm" & "pnmtopng"; 
				see back_up_3 for an example support for linux */ }
	
	if (empty($suffix))
		return false;
	
	eval('$src' . " = ImageCreateFrom$suffix('$img_orig');");
	$dst = ImageCreateTrueColor($tn_w, $tn_h);

	ImageCopyResampled($dst, $src, 0, 0, 0, 0, $tn_w, $tn_h, $w, $h);

	eval("Image$suffix(". '$dst' .", '$img_thumb');");
	if ($output_in_browser)	eval("Image$suffix(". '$dst' .");");
	
	ImageDestroy($src);
	ImageDestroy($dst);

	return true;
}

?>
