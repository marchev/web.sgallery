<table width="800" align="left" vAlign="top" border="0" cellspacing="5" cellpadding="0">
	<tr>
		<td width="5"></td>
		<td colspan="2" valign="top"><font class="title1">S-Gallery v0.1 by Svilen Marchev features:</font></td>
	</tr>
	<tr>
		<td colspan="3" height="3" valign="top"></td>
	</tr>
	<tr>
		<td width="5"></td>
		<td width="16" valign="top"><img src="_gal_files/_gal_img/li.gif"></td>
		<td>
				Thumbnail creation for JPEG, JPG, GIF and PNG image files. 
				These formats are also the default set of shown files in the gallery. 
				The set can be easily extended by editing the configuration file.
		</td>
	</tr>
	<tr>
		<td></td>
		<td valign="top"><img src="_gal_files/_gal_img/li.gif"></td>
		<td>
				No database at all! The only thing you have to do to install S-Gallery 
				is to copy its files to a desired directory. From all subfolders 
				and sub-sub-..-sub-folders of this folder can be created albums.
		</td>
	</tr>
	<tr>
		<td></td>
		<td valign="top"><img src="_gal_files/_gal_img/li.gif"></td>
		<td>
				Extremely easy album creation:<br>
				Let c:/sgal/ be the location where S-Gallery is installed.
				Suppose a folder with images somewhere in the c:/sgal/ subtree 
				(this means a subfolder or a sub-sub-...-sub-folder of c:/sgal/).
				Now to create a new album for that folder you need just to make a subfolder "_gal_thumb" 
				(linux users have to be careful about permissions). 
				You can also create a text file "_gal_info.txt" with the title of the album inside.
				And that's it!
				A link to the new album will appear in S-Gallery's main page. 
				When you open an album for first time you have to wait for the thumbnails' generation.
				The files are processed one by one, so you won't get a "time limit exceeded" error. 
				Don't worry, the procedure of thumbnails generation is done only once. :)
		</td>
	</tr>
	<tr>
		<td></td>
		<td valign="top"><img src="_gal_files/_gal_img/li.gif"></td>
		<td>
				If you want to hide a folder or a file from being shown in menu and in the albums, 
				just add prefix "_gal_" to it. Example: You want to hide a file "dream_girl.jpg". 
				Rename it to "_gal_dream_girl.jpg" and it won't appear in the album of its folder.
		</td>
	</tr>
	<tr>
		<td></td>
		<td valign="top"><img src="_gal_files/_gal_img/li.gif"></td>
		<td>
				If the gallery fails to create a thumbnail (image file is corrupted, 
				or a not supported image format is set to be supported), it automatically places 
				a "no-preview"-image. It is a link, so you can still open the desired file.
		</td>
	</tr>
	<tr>
		<td></td>
		<td valign="top"><img src="_gal_files/_gal_img/li.gif"></td>
		<td>
				If the filename of an image is too long, it is devided in some rows. 
				A "maximum file size"-protection is also included - files over that size 
				won't be processed at all. You can increase, decrease or turn off this 
				feature by editing the configuration file.
		</td>
	</tr>
</table>
