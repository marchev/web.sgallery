<?php

///////////////////////////////////////////////////////////
// Prints the contents of a gallery.
// IMPORTANT: The menu must be already built, because
//            the array $nodes is used here!
///////////////////////////////////////////////////////////

include_once("inc_conf.php");
include_once("inc_image.php");

///////////////////////////////////////////////////////////


if (empty($folder))
{
	echo "<div style='width: 800px;' align='left'>Please choose an album.</div>";
}
else
{
	$path_orig = $nodes[$folder]->link;
	$path_thumb = $nodes[$folder]->link . "_gal_thumb/";

	// scan original folder and take all images
	$d = dir($path_orig);
	$images = array();
	while (false !== ($entry = $d->read()))
		if ($entry != '.' && $entry != '..' && is_image($path_orig.$entry) && substr($entry,0,5)!="_gal_")
			$images[] = $entry;
	$d->close();

	/*
	// check if all found images have thumbnails; if not - create them;
	// woks fine, but may cause a "time limit exceeded" error;
	// you can still use it to debug the CreateThumbnail function.
	for ($i=0; $i<count($images); $i++) {
		if (!is_image($path_thumb."th_".$images[$i])) {
			$succ = CreateThumbnail($path_orig.$images[$i], $path_thumb."th_".$images[$i]);
			
			if ($succ) echo "Thumbnail successfully created.<br>";
			else echo "Failed to create thumbnail.<br>";
		}
	}
	*/
	
	// print album info
	if (!empty($nodes[$folder]->info))
		echo "<h1>". $nodes[$folder]->info ."</h1>";
	echo "Album folder: <a style='color: #3366cc;' href='$path_orig' target='_blank'>$path_orig</a><br>";
	echo "Images in album: ".count($images)."<br>";

	// sort by file name
	sort($images);
	
	// show them!
	$rows = max(1, intval((count($images)+IMAGES_PER_ROW-1) / IMAGES_PER_ROW));
	//$table_width = IMAGES_PER_ROW * IMAGE_CELL_WIDTH;
	//$table_height = $rows * IMAGE_CELL_HEIGHT;
	
	echo "<p>";
	echo "<table border='0' cellpadding='5' cellspacing='0'>";
	
	for ($r=0; $r<$rows; $r++)
	{
		echo "<tr height='".IMAGE_CELL_HEIGHT."'>";
		
		for ($c=0; $c<IMAGES_PER_ROW; $c++)
		{
			if ($r*IMAGES_PER_ROW+$c < count($images))
			{
				$img_thumb = "th_" . $images[$r*IMAGES_PER_ROW+$c];
				$img_orig = $images[$r*IMAGES_PER_ROW+$c];
				
				if (is_image($path_thumb.$img_thumb))
					list($img_thumb_w, $img_thumb_h, $img_thumb_type, $img_thumb_attr) = GetImageSize($path_thumb.$img_thumb);
				list($img_orig_w, $img_orig_h) = GetImageSize($path_orig.$img_orig);
				
				// determine the size of the original image in more user-friedly manner :)
				$img_orig_fsize = '';
				$tmp = filesize($path_orig.$img_orig);
				if ($tmp < (1<<10))
					$img_orig_fsize = $tmp . " B";
				else if ($tmp < (1<<20))
					$img_orig_fsize = ceil($tmp/(1<<10)) . " KB";
				else
					$img_orig_fsize = sprintf("%.1f",($tmp/(1<<20))) . " MB";
				
				// make the name of the image, that will be displayed below it
				$img_orig_fname = '';
				for ($k=0, $tmp=$img_orig; $k<IMAGE_FNAME_ROWS && $tmp!=''; $k++)
				{
					if (!empty($img_orig_fname)) $img_orig_fname .= '<br>';
					$img_orig_fname .= substr($tmp, 0, IMAGE_FNAME_CHARS_PER_ROW);
					$tmp = substr($tmp, IMAGE_FNAME_CHARS_PER_ROW);
				}
				if ($tmp != '')
					$img_orig_fname .= '...';
				
				
				// prepare the <img>-tag: a thumbnail or a script that creates a thumbnail
				if (is_image($path_thumb.$img_thumb))
					$img_tag = "<img src='$path_thumb$img_thumb' border='0' $img_thumb_attr>";
				else
					$img_tag = "<img src='_gal_files/makethumb.php?" 
											."path_orig=". urlencode($path_orig) ."&"
											."path_thumb=".urlencode($path_thumb)."&"
											."img_orig=".  urlencode($img_orig)  ."&"
											."img_thumb=". urlencode($img_thumb) ."' border='0'>";
				
				echo "<td width='". IMAGE_CELL_WIDTH ."' height='". IMAGE_CELL_HEIGHT ."' align='center' vAlign='middle'>
								<table width='100%' height='100%' border='0' cellpadding='0' cellspacing='0'>
									<tr>
										<td align='center' vAlign='middle'>
												<a href='$path_orig$img_orig'>$img_tag</a>
										</td>
									</tr>
									<tr>
										<td style='padding-top: 5px;' height='50' align='center' vAlign='top'>
												$img_orig_fname<br>
												$img_orig_w x $img_orig_h<br>
												$img_orig_fsize
										</td>
									</tr>
								</table>
							</td>
							";
			}
			else
			{
				echo "<td width='". IMAGE_CELL_WIDTH ."' height='". IMAGE_CELL_HEIGHT ."' align='center' vAlign='top'>&nbsp</td>";
			}
		}
		
		echo "</tr>";
	}
	
	echo "</table>";
}

?>
