<?php

/////////////////////////////////////////////////////////////////
// Page template; 
// NOTE: parameters are supposed to be global
/////////////////////////////////////////////////////////////////

if (empty($content_file))	$content_file = 'cont.php';

?>

<html>
<head>
<title>Welcome to Svilen Marchev's Photo Gallery</title>
<link rel="stylesheet" href="_gal_files/treeview.css">
<link rel="stylesheet" href="_gal_files/styles.css">
</head>


<body style="margin: 7px 10px 5px 10px">

<table width="" height="100%" bgColor="#ffffff" border="0" cellpadding="0" cellspacing="0">
	<tr height="20" valign="middle">
		<td colspan="5"><font class="title1">Welcome to Svilen Marchev's <a class="title1" href="index.php?cont=doc.php">S-Gallery</a>!</font></td>
	</tr>
	<tr><td colspan="5" bgcolor="#ffffff" height="7"></td></tr>
	<tr><td colspan="5" bgcolor="#e1e1e1" height="1"></td></tr>
	<tr><td colspan="5" bgcolor="#ffffff" height="12"></td></tr>
	<tr valign="top">
		<td width="150">
			<b>Albums by folder</b>
			<?php include('menu.php'); ?>
		</td>
		<td width="15"></td>
		<td bgcolor="#e1e1e1" valign="top" width="1"></td>
		<td width="15"></td>
		<td align="center">
			<?php
				// TODO(marchev): There is a security issue here. Whitelist all possibilities instead.
				include($content_file);
			?>
		</td>
	</tr>
	<tr><td colspan="5" bgcolor="#ffffff" height="12"></td></tr>
	<tr><td colspan="5" bgcolor="#e1e1e1" height="1"></td></tr>
	<tr><td colspan="5" bgcolor="#ffffff" height="2"></td></tr>
	<tr>
		<td colspan="5" bgcolor="#ffffff" height="15">
			<table width="100%" height="100%" bgColor="#ffffff" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="50%" align="left"><font color="#a0a0a0">S-Gallery v0.1 by Svilen Marchev</font></td>
					<td width="50%" align="right"><a style="color: #a0a0a0;" href="index.php?cont=doc.php">documentation</a></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

</body>
</html>
