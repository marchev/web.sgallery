<?php

////////////////////////////////////////////////////////////////
// S-Gallery main page
////////////////////////////////////////////////////////////////

// Which folder's album is to be opened.
$folder = intval(isset($_REQUEST['folder']) ? $_REQUEST['folder'] : '0');
// Which is the content's file.
$content_file = isset($_REQUEST['cont']) ? $_REQUEST['cont'] : '';
if (empty($content_file)) $content_file = 'cont.php';

include("_gal_files/template.php");

?>
